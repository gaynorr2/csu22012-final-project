import java.util.ArrayList;
import java.util.Arrays;

public class DijkstraShortest {

	BusNetwork busNetwork;
	BusStop currentStop;
	
	public DijkstraShortest(BusNetwork busNetwork) {
		this.busNetwork = busNetwork;
	}
	
	private int shortestFromOrigin(double[][] array, int size, boolean[] visited) {
		int index = -1;
		double shortest = Double.MAX_VALUE;
		for(int i = 0; i < size; i++) {
			if(array[0][i] < shortest && !visited[i]) {
				shortest = array[0][i];
				index = i;
			}
		}
		return index;
	}
	
	private double[][] dijsktra(int fromIndex) {
		ArrayList<BusStop> allStops = busNetwork.getStopsList();
		boolean visited[] = new boolean[allStops.size()];
		Arrays.fill(visited, false);
		int visitedCount = 0;
		
		double outputArray[][] = new double[2][allStops.size()];
		outputArray[0][fromIndex] = 0;
		outputArray[1][fromIndex] = -1;
		for(int i = 0; i < allStops.size(); i++) {
			if(i != fromIndex)
				outputArray[0][i] = Double.MAX_VALUE;
		}
		
		
		BusStop currentStop = allStops.get(fromIndex);
		while(visitedCount < allStops.size()) {
			int index = shortestFromOrigin(outputArray, allStops.size(), visited);
			if(index >= 0 && index < allStops.size())
				currentStop = allStops.get(index);
			else visitedCount = allStops.size();
			for(int i = 0; i < currentStop.edgesOutCount(); i++) {
				DirectedEdge edge = currentStop.edgesOut.get(i);
				double newWeight = outputArray[0][currentStop.index()] + edge.weight();
				if(newWeight < outputArray[0][edge.to().index()]) {
					outputArray[0][edge.to().index()] = newWeight;
					outputArray[1][edge.to().index()] = currentStop.index();
				}
			}
			visited[currentStop.index()] = true;
			visitedCount++;
		}
		return outputArray;
	}
	
	public ArrayList<String> shortestJourneyDescription(BusStop from, BusStop to) {
		double[][] shortestJourneyArray = dijsktra(from.index());
		ArrayList<String> output = new ArrayList<String>();
		int index = to.index();
		
		output.add(Double.toString(shortestJourneyArray[0][index]));
		while(index != -1) {
			output.add(busNetwork.getStopsList().get(index).formattedStopName());
			index = (int)shortestJourneyArray[1][index];
		}
		return output;
	}
}
