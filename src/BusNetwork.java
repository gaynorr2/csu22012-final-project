import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class BusNetwork {
	
	private static File stops;
	private static File stopTimes;
	private static File transfers;
	static ArrayList<BusStop> stopsList = new ArrayList<BusStop>();
	
	public BusNetwork(String inputStops, String inputStopTimes, String inputTransfers) throws FileNotFoundException {
		stops = new File(inputStops);
		stopTimes = new File(inputStopTimes);
		transfers = new File(inputTransfers);
		createNetwork();
	}
	
	private static void createNetwork() throws FileNotFoundException {
		int stopsLineCount = lineCounter(stops);
		int stopTimesLineCount = lineCounter(stopTimes);
		int transfersLineCount = lineCounter(transfers);
		double loadPercent = 0, tempCount = 0;
		int threshhold = 5;
		
		int index = 0;
		Scanner fileReader = new Scanner(stops);
		fileReader.nextLine();
		while(fileReader.hasNextLine()) {
			//stops format: stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station
			String fileLine[] = fileReader.nextLine().split(",");
			
			int stopID = Integer.parseInt(fileLine[0]);
			String stopName = fileLine[2];
		    String stopDesc = fileLine[3];
		    double stopLat = Double.parseDouble(fileLine[4]);
		    double stopLon = Double.parseDouble(fileLine[5]);
		    String zoneID = fileLine[6];
		    
		    stopsList.add(new BusStop(stopID, stopName, stopDesc, stopLat, stopLon, zoneID, index));
		    index++;
		    
		    loadPercent = (tempCount/stopsLineCount) * 100;
		    if(loadPercent > threshhold) {
		    	System.out.print("Loading stops.txt: (" + (int)loadPercent + "%)\r\n");
		    	threshhold += 5;
		    }
		    tempCount++;
		}
		tempCount = loadPercent = 0;
		threshhold = 5;
		
		fileReader.close();
		fileReader = new Scanner(stopTimes);
		fileReader.nextLine();
		
		double sameRouteWeight = 1;
		String fileLineFrom[] = fileReader.nextLine().split(",");
		while(fileReader.hasNextLine()) {
			//stopTimes format: trip_id,  arrival_time,  departure_time,  stop_id,  stop_sequence,  stop_headsign,  pickup_type,  drop_off_type,  shape_dist_traveled
			String fileLineTo[] = fileReader.nextLine().split(",");
			if(fileLineFrom[0].equalsIgnoreCase(fileLineTo[0])) {
				int fromIndex = validID(Integer.parseInt(fileLineFrom[3]));
				int toIndex = validID(Integer.parseInt(fileLineTo[3]));
				DirectedEdge edge = new DirectedEdge(stopsList.get(fromIndex), stopsList.get(toIndex), sameRouteWeight);
				stopsList.get(fromIndex).addEdge(edge);
			}
			fileLineFrom = fileLineTo;
			
			loadPercent = (tempCount/stopTimesLineCount) * 100;
		    if(loadPercent >= threshhold) {
		    	System.out.print("Loading stop_times.txt: (" + (int)loadPercent + "%)\r\n");
		    	threshhold += 5;
		    }
		    tempCount++;
		}
		tempCount = loadPercent = 0;
		threshhold = 5;
		
		fileReader.close();
		fileReader = new Scanner(transfers);
		fileReader.nextLine();
		while(fileReader.hasNextLine()) {
			//transfers format: from_stop_id,to_stop_id,transfer_type,min_transfer_time
			String fileLine[] = fileReader.nextLine().split(",");
			int fromIndex = validID(Integer.parseInt(fileLine[0]));
			int toIndex = validID(Integer.parseInt(fileLine[1]));
			int transferType = Integer.parseInt(fileLine[2]);
			double transferWeight = 2;
			
			if(transferType == 2) {
				int minTransferTime = Integer.parseInt(fileLine[3]);
				transferWeight = (double)minTransferTime / 100.0;
			}
			DirectedEdge edge = new DirectedEdge(stopsList.get(fromIndex), stopsList.get(toIndex), transferWeight);
			stopsList.get(fromIndex).addEdge(edge);
			
			loadPercent = (tempCount/transfersLineCount) * 100;
		    if(loadPercent > threshhold) {
		    	System.out.print("Loading transfers.txt: (" + (int)loadPercent + "%)\r\n");
		    	threshhold += 5;
		    }
		    tempCount++;
		}
		fileReader.close();
	}
	
	private static int lineCounter(File file) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(file);
		int lineCount = 0;
		while(fileScanner.hasNextLine()) {
			fileScanner.nextLine();
			lineCount++;
		}
		fileScanner.close();
		return lineCount;
	}
	
	public static int validID(int stopID) {
		int output = -1;
		for(int i = 0; i < stopsList.size(); i++) {
			if(stopsList.get(i).stopID() == stopID) {
				output = i;
			}
		}
		return output;
	}
	
	public ArrayList<BusStop> getStopsList() {
		return stopsList;
	}
}
