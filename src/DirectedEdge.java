
public class DirectedEdge {
	private BusStop from, to;
	private double weight;
	
	public DirectedEdge(BusStop from, BusStop to, double weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
	
	public BusStop from() {
		return from;
	}
	
	public BusStop to() {
		return to;
	}
	
	public double weight() {
		return weight;
	}
}
