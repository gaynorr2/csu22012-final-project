import java.util.ArrayList;

public class BusStop {
	private int stopID, index;
	double stopLon, stopLat;
	private String stopName, stopDesc, zoneID, niceStopName;
	ArrayList<DirectedEdge> edgesOut = new ArrayList<DirectedEdge>();
	
	
	public BusStop(int stopID, String stopName, String stopDesc, double stopLat, double stopLon, String zoneID, int index) {
		this.index = index;
		this.stopID = stopID;
		this.stopName = stopName;
		this.stopDesc = stopDesc;
		this.stopLat = stopLat;
		this.stopLon = stopLon;
		this.zoneID = zoneID;
		
		niceStopName = reformatStopName(stopName);
	}
	
	private static String reformatStopName(String stopName) {
		String temp = stopName.substring(0, stopName.indexOf(' '));
		if(temp.equalsIgnoreCase("EB") || temp.equalsIgnoreCase("WB") ||temp.equalsIgnoreCase("NB") ||temp.equalsIgnoreCase("SB") ||temp.equalsIgnoreCase("FLAGSTOP"))
			stopName = stopName.substring(stopName.indexOf(' ') + 1) + (" " + temp);
		return stopName;
	}
	
	public void addEdge(DirectedEdge edge) {
		edgesOut.add(edge);
	}
	
	public int edgesOutCount() {
		return edgesOut.size();
	}
	
	public int stopID() {
		return stopID;
	}

    public int index() {
    	return index;
    }
	
	public String formattedStopName() {
		return niceStopName;
	}
	
	public String formattedStopInfo() {
		String returnString = "\r\nstop ID: " + stopID + " stop name: " + stopName + "stop desc: " + stopDesc + "stop lat: " + stopLat + "stop lon: " + stopLon + "zone ID: " + zoneID;
		return returnString;
	}
}
