import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Vancouver_Bus_Management_System {
	
	private static Scanner inputScanner = new Scanner(System.in);
	static BusNetwork vancouverBusNetwork;
	
	public static void main(String[] args) throws FileNotFoundException {
		vancouverBusNetwork = new BusNetwork("stops.txt", "stop_times.txt", "transfers.txt");
		
		boolean exit = false;
		printTitleScreen();
		while(!exit) {
			switch(navigateMenu(0, 0)) {
			    case 1:
			    	if(shortestJourney()) {
			    		printInstructions(0,3);
			    		exit = true;
			    	}
			    	break;
                case 2:
                	if(stopInfo()) {
                		printInstructions(0,3);
                		exit = true;
                	}
			    	break;
                case 3:
                	if(optionThree()) {
                		printInstructions(0,3);
                		exit = true;
                	}
	                break;
                case 4:
                	printInstructions(0,3);
                	exit = true;
	                break;
			}
			//Return to Main Menu
		}
		inputScanner.close();
		//Exit Program
	}
	
	private static boolean shortestJourney() {
		boolean exit = false;
		while(!exit) {
			int menuReturn, firstStop, secondStop;
			
			menuReturn = navigateMenu(1,0);
			if(menuReturn == -1)
				return true;
			else firstStop = menuReturn;
			
			menuReturn = navigateMenu(1,1);
			if(menuReturn == -1)
				return true;
			else secondStop = menuReturn;
			
			DijkstraShortest dijkstra = new DijkstraShortest(vancouverBusNetwork);
			ArrayList<String> journeyDescription = dijkstra.shortestJourneyDescription (vancouverBusNetwork.getStopsList().get(firstStop), vancouverBusNetwork.getStopsList().get(secondStop));
			printShortestJourney(journeyDescription);
			
			switch(navigateMenu(0, 1)) {
			case 1:  //Return to Main Menu
				return false;
			case 2:  //Exit Program
				return true;
			default: //Continue with Option 1
			}
		}
		//never executed code:
		return false;
	}

	private static boolean stopInfo() {
		printInstructions(2, 0);
		printInstructions(2, 1);
		return true;
	}
	
	private static boolean optionThree() {
		printInstructions(3, 0);
		printInstructions(3, 1);
		return true;
	}
	
	private static int validStopID(String userInput) {
		int stopIndex = -1;
		if(userInput.matches("-?(0|[1-9]\\d*)")) {
			stopIndex = vancouverBusNetwork.validID(Integer.parseInt(userInput));
			if(stopIndex == -1) {
				printError(2);
				return stopIndex;
			}
			return stopIndex;
		}
		else printError(2);
		return stopIndex;
	}
	
	private static int navigateMenu(int index, int innerIndex) {
		switch(index){
		case 0: {
			switch(innerIndex) {
			case 0: {
				printInstructions(0,0);
				boolean validInput = false;
				int optionSelected = 0;
				while(!validInput) {
					printInstructions(0, 1);
					if(inputScanner.hasNextInt()) {
						optionSelected = inputScanner.nextInt();
						if(optionSelected >= 1 && optionSelected <= 4) {
							validInput = true;
						}
						else printError(0);
					}
					else {
						printError(0);
						inputScanner.next();
					}
				}
				return optionSelected;
			}
			
			case 1: {
				printInstructions(0, 2);
				boolean validInput = false;
				while(!validInput) {
					String input = inputScanner.next();
					if(input.equalsIgnoreCase("yes")) {
						return 0;
					}
					else if(input.equalsIgnoreCase("back")) {
						return 1;
					}
					else if(input.equalsIgnoreCase("exit")) {
						return 2;
					}
					else printError(3);
				}
			}
			}
		}
			
		case 1: {
			switch (innerIndex) {
			case 0: {
				boolean validInput = false;
				printInstructions(1, 0);
				String input;
				while(!validInput) {
					printInstructions(1, 1);
					input = inputScanner.next();
					if(input.equalsIgnoreCase("exit"))
						return -1;
					int returnIndex = validStopID(input);
					if(returnIndex != -1) 
						return returnIndex;
				}
			}
			
			case 1: {
				boolean validInput = false;
				String input;
				while(!validInput) {
					printInstructions(1,2);
					input = inputScanner.next();
					if(input.equalsIgnoreCase("exit"))
						return -1;
					int returnIndex = validStopID(input);
					if(returnIndex != -1) 
						return returnIndex;
				}
			}
			}
		}
		default: return 0;
		}
	}
	
	private static void printTitleScreen(){
		String titleScreen = "\r\n" +
	            "              ______________________________________________________________________________________________________________\r\n" +
				"                   __          __         _                                       _               _     _                                                    \r\n" + 
				"                   \\ \\        / /        | |                                     | |             | |   | |                                                   \r\n" + 
				"                    \\ \\  /\\  / /    ___  | |   ___    ___    _ __ ___     ___    | |_    ___     | |_  | |__     ___                                         \r\n" + 
				"                     \\ \\/  \\/ /    / _ \\ | |  / __|  / _ \\  | '_ ` _ \\   / _ \\   | __|  / _ \\    | __| | '_ \\   / _ \\                                        \r\n" + 
				"                      \\  /\\  /    |  __/ | | | (__  | (_) | | | | | | | |  __/   | |_  | (_) |   | |_  | | | | |  __/                                        \r\n" + 
				"                       \\/  \\/      \\___| |_|  \\___|  \\___/  |_| |_| |_|  \\___|    \\__|  \\___/     \\__| |_| |_|  \\___|                                        \r\n" + 
				"                                                                                                                                                             \r\n" + 
				"                                                                                                                                                             \r\n" + 
				"                               __      __                                                                                                                    \r\n" + 
				"                               \\ \\    / /                                                                                                                    \r\n" + 
				"                                \\ \\  / /    __ _   _ __     ___    ___    _   _  __   __   ___   _ __                                                        \r\n" + 
				"                                 \\ \\/ /    / _` | | '_ \\   / __|  / _ \\  | | | | \\ \\ / /  / _ \\ | '__|                                                       \r\n" + 
				"                                  \\  /    | (_| | | | | | | (__  | (_) | | |_| |  \\ V /  |  __/ | |                                                          \r\n" + 
				"                                   \\/      \\__,_| |_| |_|  \\___|  \\___/   \\__,_|   \\_/    \\___| |_|                                                          \r\n" + 
				"                                                                                                                                                             \r\n" + 
				"                                                                                                                                                             \r\n" + 
				"  ____                    __  __                                                                     _        _____                 _                        \r\n" + 
				" |  _ \\                  |  \\/  |                                                                   | |      / ____|               | |                       \r\n" + 
				" | |_) |  _   _   ___    | \\  / |   __ _   _ __     __ _    __ _    ___   _ __ ___     ___   _ __   | |_    | (___    _   _   ___  | |_    ___   _ __ ___    \r\n" + 
				" |  _ <  | | | | / __|   | |\\/| |  / _` | | '_ \\   / _` |  / _` |  / _ \\ | '_ ` _ \\   / _ \\ | '_ \\  | __|    \\___ \\  | | | | / __| | __|  / _ \\ | '_ ` _ \\   \r\n" + 
				" | |_) | | |_| | \\__ \\   | |  | | | (_| | | | | | | (_| | | (_| | |  __/ | | | | | | |  __/ | | | | | |_     ____) | | |_| | \\__ \\ | |_  |  __/ | | | | | |  \r\n" + 
				" |____/   \\__,_| |___/   |_|  |_|  \\__,_| |_| |_|  \\__,_|  \\__, |  \\___| |_| |_| |_|  \\___| |_| |_|  \\__|   |_____/   \\__, | |___/  \\__|  \\___| |_| |_| |_|  \r\n" + 
				"                                                            __/ |                                                      __/ |                                 \r\n" + 
				"                                                           |___/                                                      |___/                                  \r\n" + 
				"_______________________________________________________________________________________________________________________________________________________________";
		System.out.print(titleScreen);
	}
	
	private static void printInstructions(int option, int index) {
		switch(option) {
		case 0:
			String mainInstructions[] = new String[4];
			mainInstructions[0] = "\r\n" + "\r\n" +  "\r\n" +
					"__________________________________________________________________________________ \r\n" +
					"|    |                                                                            | \r\n" +
					"| 1. | Obtain a detailed description of the shortest route between two bus stops  | \r\n" +
					"|____|____________________________________________________________________________| \r\n" +
					"|    |                                                                            | \r\n" +
					"| 2. | Obtain detailed information about a particular bus stop(s)                 | \r\n" +
					"|____|____________________________________________________________________________| \r\n" +
					"|    |                                                                            | \r\n" +
					"| 3. | Obtain detailed information on all trips arriving at a particular time     | \r\n" +
					"|____|____________________________________________________________________________| \r\n" +
					"|    |                                                                            | \r\n" +
					"| 4. | Quit and exit the application                                              | \r\n" +
					"|____|____________________________________________________________________________| \r\n" +
					"\r\n";
					
			mainInstructions[1] = "Please choose an option from the list above (enter the associated number): ";
			
			mainInstructions[2] = 
    				"\r\n" +
    	        	"To make another search, enter \"yes\". To return to the main menu, enter \"back\". To quit and exit the application, enter \"exit\": ";
			
			mainInstructions[3] = "\r\n" +
					"__________________________________________________________________________________ \r\n" +
					"|                                                                                 | \r\n" +
					"|                                 - Goodbye! -                                    | \r\n" +
					"|_________________________________________________________________________________| \r\n";
			
			System.out.print(mainInstructions[index]);
			break;
			
        case 1:
        	String oneInstructions[] = new String[3];
        	oneInstructions[0] = "\r\n" +
    				"       __________________________________________________________________________________ \r\n" +
    				"       |                                                                                 | \r\n" +
    				"       | - Obtain a detailed description of the shortest route between two bus stops -   | \r\n" +
    				"       |_________________________________________________________________________________| \r\n" +
    				"\r\n";
        	
        	oneInstructions[1] = "Please enter the stop id of the first bus stop (or enter \"exit\" to quit and exit the application): ";
        	
        	oneInstructions[2] = "\r\nPlease enter the stop id of the second bus stop (or enter \"exit\" to quit and exit the application): ";
        	
        	System.out.print(oneInstructions[index]);
			break;
			
        case 2:
        	String twoInstructions[] = new String[2];
        	twoInstructions[0] = "\r\n" +
    				"       __________________________________________________________________________________ \r\n" +
    				"       |                                                                                 | \r\n" +
    				"       |         - Obtain detailed information about a particular bus stop(s) -          | \r\n" +
    				"       |_________________________________________________________________________________| \r\n" +
    				"\r\n";
        	
        	twoInstructions[1] = "Please search the name of a bus stop (or enter \"exit\" to quit and exit the application): ";
        	
        	System.out.print(twoInstructions[index]);
	        break;
	        
        case 3:
        	String threeInstructions[] = new String[2];
        	threeInstructions[0] = "\r\n" +
    				"       __________________________________________________________________________________ \r\n" +
    				"       |                                                                                 | \r\n" +
    				"       |   - Obtain detailed information on all trips arriving at a particular time -    | \r\n" +
    				"       |_________________________________________________________________________________| \r\n" +
    				"\r\n";
        	
        	threeInstructions[1] = "Please enter a time of arrival (or enter \"exit\" to quit and exit the application): ";
        	
        	System.out.print(threeInstructions[index]);
	        break;
		}
	}

	private static void printError(int index) {
		String errorMessages[] = new String[4];
		
		errorMessages[0] = "\r\nInvalid input! ";
		errorMessages[1] = "\r\nInvalid input! Please enter a number between 1-4 to choose an option form the list above: ";
		errorMessages[2] = "\r\nInvalid Bus Stop ID! ";
		errorMessages[3] = "\r\nInvalid input! To make another search, enter \"yes\". To return to the main menu, enter \"back\". To quit and exit the application, enter \"exit\": ";
		
		System.out.print(errorMessages[index]);
	}

	private static void printShortestJourney(ArrayList<String> journeyDescription) {
		double cost = Double.parseDouble(journeyDescription.get(0));
		String from = journeyDescription.get(journeyDescription.size() - 1);
		String to = journeyDescription.get(1);
		String journeyDescriptionString = "\r\n" +
				"__________________________________________________________________________________ \r\n" +
				"|                                                                                  \r\n" +
				"|      - Shortest route between " + from + " and " + to + ", with a \"cost\" of " + cost + "             \r\n" +
				"|_________________________________________________________________________________ \r\n";
		
		for(int i = 1; i < journeyDescription.size(); i++) {
			if(i < 10)
				journeyDescriptionString += 
					"_____\r\n" +
			        "|    |\r\n" +
					"| " + i + ". | " + journeyDescription.get(journeyDescription.size() - i) + " \r\n" +
			        "|____|\r\n";
			else
				journeyDescriptionString += 
				"______\r\n" +
		        "|     |\r\n" +
				"| " + i + ". | " + journeyDescription.get(journeyDescription.size() - i) + " \r\n" +
		        "|_____|\r\n";
		}
		System.out.print(journeyDescriptionString);
	}

}
